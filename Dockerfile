FROM python:3.8

# Grab requirements.txt.
ADD ./webapp/requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip3 install -r /tmp/requirements.txt


# Add our code
ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp/amp
RUN pip install .
WORKDIR /opt/webapp

RUN pip list

CMD gunicorn --bind 0.0.0.0:5000 wsgi