from flask import Flask, jsonify, request
import json
import numpy as np
from amp.inference import HYDRAmpGenerator
app = Flask(__name__)
generator = HYDRAmpGenerator('/opt/webapp/amp/models/faster_annealing/OldHydraWithoutMICGrad_WithAMPSobolev','/opt/webapp/amp/models/decomposer.joblib')


@app.route('/amp/predict', methods = ['POST'])
def unconstrained_generation():
    return jsonify({'prediction': generator.unconstrained_generation(n_target=2)})

if __name__ == '__main__':
    app.run(host='0.0.0.0')
