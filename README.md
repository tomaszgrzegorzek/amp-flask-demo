
### Docker

Sync submodules (to get amp package)

Build Docker image with command ```docker build -t deploy-keras-model-flask .```

Run built Docker image with ```docker run --name keras-flask -p 5555:5000 deploy-keras-model-flask```.

To test:

```powershell
# TESTED, POWERSHELL

Invoke-WebRequest -Uri http://localhost:5555/amp/predict -Method POST -UseBasicParsing
```
```bash
# should work , did not test
curl -X POST \
  http://localhost:5000/amp/predict \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' 

```